# frozen_string_literal: true

module OwmCurrentWeather
  class Engine < ::Rails::Engine
    isolate_namespace OwmCurrentWeather
    config.generators.api_only = true

    DEFAULT_STYLES = { font_size: 400 }.freeze
  end
end
