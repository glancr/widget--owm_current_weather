# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'owm_current_weather/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'owm_current_weather'
  s.version     = OwmCurrentWeather::VERSION
  s.authors     = ['Tobias Grasse', 'Marco Roth']
  s.email       = ['tg@glancr.de', 'marco.roth@intergga.ch']
  s.homepage    = 'https://glancr.de/modules/owm_current_weather'
  s.summary     = 'Widget for mirr.OS to show the current weather conditions from Openweathermap sources.'
  s.description = 'Shows the conditions for the current 3-hour period from the Openweathermap source.'
  s.license     = 'MIT'
  s.metadata    = {
    'json' => {
      type: 'widgets',
      title: {
        enGb: 'Live weather',
        deDe: 'Livewetter',
        frFr: 'Météo en direct',
        esEs: 'Clima en vivo',
        plPl: 'Żywa pogoda',
        koKr: '라이브 날씨'
      },
      description: {
        enGb: s.description,
        deDe: 'Zeigt die aktuelle Wetterlage der nächsten drei Stunden aus der Openweathermap-Datenquelle.',
        frFr: 'Affiche les conditions pour la période actuelle de 3 heures à partir de la source Openweathermap.',
        esEs: 'Muestra las condiciones para el período actual de 3 horas desde la fuente Openweathermap.',
        plPl: 'Pokazuje warunki dla bieżącego 3-godzinnego okresu ze źródła Openweathermap.',
        koKr: 'Openweathermap 소스에서 현재 3 시간 동안의 조건을 표시합니다.'
      },
      sizes: [
        {
          w: 3,
          h: 2
        },
        {
          w: 4,
          h: 2
        },
        {
          w: 12,
          h: 6
        },
        {
          w: 12,
          h: 12
        },
        {
          w: 21,
          h: 3
        },
        {
          w: 21,
          h: 12
        }
      ],
      languages: %i[enGb deDe frFr esEs plPl koKr],
      group: 'current_weather',
      compatibility: '0.11.0',
      single_source: true
    }.to_json
  }

  s.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']

  s.add_development_dependency 'rails', '~> 5.2'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rubocop-rails'
end
