# frozen_string_literal: true

require 'test_helper'

module OwmCurrentWeather
  class Test < ActiveSupport::TestCase
    test 'truth' do
      assert_kind_of Module, OwmCurrentWeather
    end
  end
end
